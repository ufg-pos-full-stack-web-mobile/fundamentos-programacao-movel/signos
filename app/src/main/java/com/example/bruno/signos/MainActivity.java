package com.example.bruno.signos;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private ListView listViewSigno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewSigno = findViewById(R.id.list_view_id);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                Signos.getSignosName()
        );

        listViewSigno.setAdapter( adapter );

        listViewSigno.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String signo = (String) listViewSigno.getItemAtPosition(position);
                String perfil = Signos.getCaracteristicaFromName(signo);

                Toast.makeText(getApplicationContext(), perfil, Toast.LENGTH_LONG).show();
            }
        });
    }
}
