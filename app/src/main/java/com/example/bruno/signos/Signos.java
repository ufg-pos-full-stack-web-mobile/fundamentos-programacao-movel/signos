package com.example.bruno.signos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno on 15/02/18.
 */

public enum Signos {

    ARIES("Áries", "Arianos são animados..."),
    TOURO("Touro", "Touros são chifrudos..."),
    GEMEOS("Gâmeos", "Gêmeos são parecidos"),
    CANCER("Câncer", "Sem piada infâme"),
    LEAO("Leão", "Leão é bravo"),
    VIRGEM("Virgem", "Virgem é infeliz"),
    LIBRA("Libra", "Libra é ... sei lá"),
    ESCORPIAO("Escorpião", "Escorpião é venenoso"),
    SARGITARIO("Sargitário", "Sargitário ensinou o Cólera do Dragão para o Shyriu"),
    CAPRICORNIO("Capricórnio", "Não faço ideia também"),
    AQUARIO("Aquário", "Serve para criar peixes"),
    PEIXES("Peixes", "Peixes nadam");

    private final String nome;
    private final String perfil;

    Signos(String nome, String perfil) {
        this.nome = nome;
        this.perfil = perfil;
    }

    public static String[] getSignosName() {
        int i = 0;
        String[] signos = new String[12];
        for (Signos signo : Signos.values()) {
            signos[i++] = signo.nome;
        }
        return signos;
    }
    
    public static String getCaracteristicaFromName(String name) {
        for (Signos signo : Signos.values()) {
            if (name.equals(signo.nome)) {
                return signo.perfil;
            }         
        }
        return "Signo não identificado";
    }
}
